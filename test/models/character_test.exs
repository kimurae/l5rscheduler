defmodule L5rScheduler.CharacterTest do
  use L5rScheduler.ModelCase

  alias L5rScheduler.{Character, GameSchedule}

  @invalid_attrs %{}

  defp game_schedule_id do
    {:ok, game_schedule} = GameSchedule.changeset(%GameSchedule{}, %{day: "some_content", label: "Some Content", time_of_day: %{hour: 14, min: 0, sec: 0}})
      |> Repo.insert

    game_schedule.id
  end

  defp valid_attrs do
    %{clan: "some content",
      game_schedule_id: game_schedule_id,
      name: "some content",
      type: "some content"
    }
  end

  test "changeset with valid attributes" do
    changeset = Character.changeset(%Character{}, valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Character.changeset(%Character{}, @invalid_attrs)
    refute changeset.valid?
  end
end
