defmodule L5rScheduler.UserTest do
  use L5rScheduler.ModelCase

  alias L5rScheduler.User

  @valid_attrs %{password: "some content", username: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "registration changeset with invalid shared secret" do
    changeset = User.registration_changeset(%User{}, Map.put(@valid_attrs, :shared_secret, "mom"))
    refute changeset.valid?
  end

  test "registration changeset with valid shared secret" do
    changeset = User.registration_changeset(%User{}, Map.put(@valid_attrs, :shared_secret, "thingsfallapart"))
    assert changeset.valid?
  end

  test "registration changeset without shared secret" do
    changeset = User.registration_changeset(%User{}, @valid_attrs)
    refute changeset.valid?
  end
end
