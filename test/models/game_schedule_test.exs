defmodule L5rScheduler.GameScheduleTest do
  use L5rScheduler.ModelCase

  alias L5rScheduler.GameSchedule

  @valid_attrs %{day: "some content", label: "some content", time_of_day: %{hour: 14, min: 0, sec: 0}}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = GameSchedule.changeset(%GameSchedule{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = GameSchedule.changeset(%GameSchedule{}, @invalid_attrs)
    refute changeset.valid?
  end
end
