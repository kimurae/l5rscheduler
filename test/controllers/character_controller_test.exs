defmodule L5rScheduler.CharacterControllerTest do
  use L5rScheduler.ConnCase
  require IEx

  alias L5rScheduler.{Character,GameSchedule,User}

  @valid_attrs %{clan: "some content", name: "some content", type: "some content"}
  @invalid_attrs %{}

  @secret String.duplicate("abcdef0123456789", 8)

  @default_opts [
    store: :cookie,
    key: "foobar",
    encryption_salt: "encrypted cookie salt",
    signing_salt: "signing salt"
  ]

  setup do
    {:ok, game_schedule} = GameSchedule.changeset(%GameSchedule{}, %{day: "some_content", label: "Some Content", time_of_day: %{hour: 14, min: 0, sec: 0}})
      |> Repo.insert

    valid_attrs = Map.put(@valid_attrs, :game_schedule_id, game_schedule.id)

    user = Repo.insert! %User{username: "meh", password: "meh"}
    conn = build_conn
            |> sign_conn()
            |> Guardian.Plug.sign_in(user, :access)
    {:ok, conn: conn, valid_attrs: valid_attrs}
  end

  defp sign_conn(conn) do
    put_in(conn.secret_key_base, @secret)
    |> Plug.Session.call(Plug.Session.init(Keyword.put(@default_opts, :encrypt, false)))
    |> fetch_session
  end

  defp current_user(conn) do
    Guardian.Plug.current_resource(conn)
  end

  test "lists all entries on index", %{conn: conn, valid_attrs: _valid_attrs} do
    conn = get conn, character_path(conn, :index)
    assert html_response(conn, 200) =~ "Characters"
  end

  test "renders form for new resources", %{conn: conn, valid_attrs: _valid_attrs} do
    conn = get conn, character_path(conn, :new)
    assert html_response(conn, 200) =~ "New character"
  end

  test "creates resource and redirects when data is valid", %{conn: conn, valid_attrs: valid_attrs} do
    conn = post conn, character_path(conn, :create), character: valid_attrs
    assert redirected_to(conn) == character_path(conn, :index)
    assert Repo.get_by(Character, valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn, valid_attrs: _valid_attrs} do
    conn = post conn, character_path(conn, :create), character: @invalid_attrs
    assert html_response(conn, 200) =~ "New character"
  end

  test "shows chosen resource", %{conn: conn, valid_attrs: valid_attrs} do
    character = current_user(conn)
      |> build_assoc(:characters)
      |> Character.changeset(valid_attrs)
      |> Repo.insert!

    conn = get conn, character_path(conn, :show, character)
    assert html_response(conn, 200) =~ "Show character"
  end

  test "renders page not found when id is nonexistent", %{conn: conn, valid_attrs: _valid_attrs} do
    assert_error_sent 404, fn ->
      get conn, character_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn, valid_attrs: _valid_attrs} do
    character = Guardian.Plug.current_resource(conn)
      |> build_assoc(:characters)
      |> Repo.insert!

    conn = get conn, character_path(conn, :edit, character)
    assert html_response(conn, 200) =~ "Edit character"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn, valid_attrs: valid_attrs} do
    character = Guardian.Plug.current_resource(conn)
      |> build_assoc(:characters)
      |> Repo.insert!

    conn = put conn, character_path(conn, :update, character), character: valid_attrs
    assert redirected_to(conn) == character_path(conn, :show, character)
    assert Repo.get_by(Character, valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn, valid_attrs: _valid_attrs} do
    character = Guardian.Plug.current_resource(conn)
      |> build_assoc(:characters)
      |> Repo.insert!

    conn = put conn, character_path(conn, :update, character), character: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit character"
  end

  test "deletes chosen resource", %{conn: conn, valid_attrs: _valid_attrs} do
    character = Guardian.Plug.current_resource(conn)
      |> build_assoc(:characters)
      |> Repo.insert!

    conn = delete conn, character_path(conn, :delete, character)
    assert redirected_to(conn) == character_path(conn, :index)
    refute Repo.get(Character, character.id)
  end
end
