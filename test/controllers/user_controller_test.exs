defmodule L5rScheduler.UserControllerTest do
  use L5rScheduler.ConnCase

  alias L5rScheduler.User

  @secret String.duplicate("abcdef0123456789", 8)
  @valid_attrs %{password: "some content", username: "some content", shared_secret: "thingsfallapart"}
  @invalid_attrs %{}

  @default_opts [
    store: :cookie,
    key: "foobar",
    encryption_salt: "encrypted cookie salt",
    signing_salt: "signing salt"
  ]

  setup do
    user = Repo.insert! %User{}
    conn = build_conn
            |> sign_conn()
            |> Guardian.Plug.sign_in(user, :access)
    {:ok, conn: conn}
  end

  defp sign_conn(conn) do
    put_in(conn.secret_key_base, @secret)
    |> Plug.Session.call(Plug.Session.init(Keyword.put(@default_opts, :encrypt, false)))
    |> fetch_session
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, user_path(conn, :new)
    assert html_response(conn, 200) =~ "Create Account"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, user_path(conn, :create), user: @valid_attrs
    assert redirected_to(conn) == game_schedule_path(conn, :index)
    assert Repo.get_by(User, %{username: "some content"})
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, user_path(conn, :create), user: @invalid_attrs
    assert html_response(conn, 200) =~ "Create Account"
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    conn = get conn, user_path(conn, :edit, 1)
    assert html_response(conn, 200) =~ "Edit Profile"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    conn = put conn, user_path(conn, :update, 1), user: @valid_attrs
    assert redirected_to(conn) == game_schedule_path(conn, :index)
    assert Repo.get_by(User, %{username: "some content"})
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    user = Repo.insert! %User{}
    conn = put conn, user_path(conn, :update, user), user: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit Profile"
  end
end
