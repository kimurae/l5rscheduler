defmodule L5rScheduler.GameScheduleControllerTest do
  use L5rScheduler.ConnCase

  alias L5rScheduler.{GameSchedule,User}
  @valid_attrs %{day: "some content", label: "some content", time_of_day: %{hour: 14, min: 0, sec: 0}}
  @invalid_attrs %{}

  @secret String.duplicate("abcdef0123456789", 8)

  @default_opts [
    store: :cookie,
    key: "foobar",
    encryption_salt: "encrypted cookie salt",
    signing_salt: "signing salt"
  ]

  setup do
    user = Repo.insert! %User{username: "meh", password: "meh"}
    conn = build_conn
            |> sign_conn()
            |> Guardian.Plug.sign_in(user, :access, perms: %{gm: Guardian.Permissions.max})
    {:ok, conn: conn}
  end

  defp sign_conn(conn) do
    put_in(conn.secret_key_base, @secret)
    |> Plug.Session.call(Plug.Session.init(Keyword.put(@default_opts, :encrypt, false)))
    |> fetch_session
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, game_schedule_path(conn, :index)
    assert html_response(conn, 200) =~ "Schedule"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, game_schedule_path(conn, :new)
    assert html_response(conn, 200) =~ "New game schedule"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, game_schedule_path(conn, :create), game_schedule: @valid_attrs
    assert redirected_to(conn) == game_schedule_path(conn, :index)
    assert Repo.get_by(GameSchedule, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, game_schedule_path(conn, :create), game_schedule: @invalid_attrs
    assert html_response(conn, 200) =~ "New game schedule"
  end

  test "shows chosen resource", %{conn: conn} do
    game_schedule = Repo.insert! %GameSchedule{}
    conn = get conn, game_schedule_path(conn, :show, game_schedule)
    assert html_response(conn, 200) =~ "Show game schedule"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, game_schedule_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    game_schedule = Repo.insert! %GameSchedule{}
    conn = get conn, game_schedule_path(conn, :edit, game_schedule)
    assert html_response(conn, 200) =~ "Edit game schedule"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    game_schedule = Repo.insert! %GameSchedule{}
    conn = put conn, game_schedule_path(conn, :update, game_schedule), game_schedule: @valid_attrs
    assert redirected_to(conn) == game_schedule_path(conn, :show, game_schedule)
    assert Repo.get_by(GameSchedule, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    game_schedule = Repo.insert! %GameSchedule{}
    conn = put conn, game_schedule_path(conn, :update, game_schedule), game_schedule: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit game schedule"
  end

  test "deletes chosen resource", %{conn: conn} do
    game_schedule = Repo.insert! %GameSchedule{}
    conn = delete conn, game_schedule_path(conn, :delete, game_schedule)
    assert redirected_to(conn) == game_schedule_path(conn, :index)
    refute Repo.get(GameSchedule, game_schedule.id)
  end
end
