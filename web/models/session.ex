defmodule L5rScheduler.Session do
  import Comeonin.Bcrypt, only: [checkpw: 2]

  alias L5rScheduler.{Repo, User}

  def authenticate([username: username, password: password]) do
    user = Repo.get_by(User, username: username)

    if user && checkpw(password, user.password_hash) do
      {:ok, user}
    else
      {:error, "Username and password do not match"}
    end
  end
end
