defmodule L5rScheduler.User do
  use L5rScheduler.Web, :model

  schema "users" do
    field :password, :string, virtual: true
    field :shared_secret, :string, virtual: true

    field :admin, :boolean
    field :username, :string
    field :password_hash, :string

    has_many :characters, L5rScheduler.Character

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, ~w(admin username password))
    |> encrypt_password
    |> validate_required([:username, :password_hash])
    |> unique_constraint(:username)
  end

  def registration_changeset(struct, params \\ %{}) do
    changeset(struct, params)
    |> cast(params, ~w(shared_secret))
    |> validate_inclusion(:shared_secret, [Application.get_env(:l5r_scheduler, :shared_secret)])
    |> validate_required([:shared_secret])
  end

  defp encrypt_password(%Ecto.Changeset{changes: %{password: password}} = changeset), do: put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(password))

  defp encrypt_password(changeset), do: changeset
end
