defmodule L5rScheduler.GameSchedule do
  use L5rScheduler.Web, :model

  schema "game_schedules" do
    field :day, :string
    field :label, :string
    field :name, :string, virtual: true
    field :time_of_day, Ecto.Time

    has_many :characters, L5rScheduler.Character

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:label, :day, :time_of_day])
    |> validate_required([:label, :day, :time_of_day])
  end

  def names_and_ids(query) do
    from c in query,
      select: {fragment("? || ' (' || ? || ')' AS name", field(c, :day), field(c, :label)), c.id},
      order_by: fragment("name")
  end
end
