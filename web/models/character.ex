defmodule L5rScheduler.Character do
  use L5rScheduler.Web, :model

  schema "characters" do
    field :avatar,   :string
    field :clan,     :string
    field :keywords, {:array, :string}
    field :name,     :string
    field :type,     :string

    belongs_to :game_schedule, L5rScheduler.GameSchedule
    belongs_to :user,          L5rScheduler.User

    timestamps()
  end

  def by_username(query) do
    from c in query,
    join: u in assoc(c, :user),
    order_by: u.username,
    preload: [user: u]
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:clan, :game_schedule_id, :keywords, :name, :type])
    |> validate_required([:clan, :game_schedule_id, :name, :type])
  end
end
