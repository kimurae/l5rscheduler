defmodule L5rScheduler.ArrayHelper do

  def array_values_for(form, field), do: array_values_for(Phoenix.HTML.Form.input_value(form, field))
  def array_values_for(list) when is_list(list), do: Enum.with_index(list)
  def array_values_for(list), do: Enum.with_index([])
end
