defmodule L5rScheduler.LoginHelper do
  def current_user(conn),     do: Guardian.Plug.current_resource(conn)
  def logged_in?(conn),       do: Guardian.Plug.authenticated?(conn)
  def owner?(conn, resource), do: current_user(conn).id == resource.user_id
end
