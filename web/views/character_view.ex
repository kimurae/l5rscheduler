defmodule L5rScheduler.CharacterView do
  use L5rScheduler.Web, :view

  def clans do
    ~w(
      Badger
      Bat
      Boar
      Chameleon
      Crab
      Crane
      Dragon
      Dragonfly
      Hare
      Imperial
      Lion
      Mantis
      Monkey
      Oriole
      Ox
      Phoenix
      Scorpion
      Sparrow
      Tortoise
      Unicorn
      Ronin
      Spider
    )
  end

  def game_schedules do
    L5rScheduler.GameSchedule
      |> L5rScheduler.GameSchedule.names_and_ids
      |> L5rScheduler.Repo.all
  end

  def types do
    ~w(
      Artisan
      Bushi
      Courtier
      Monk
      Ninja
      Shugenja
    )
  end

end
