defmodule L5rScheduler.Router do
  use L5rScheduler.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.LoadResource
  end

  pipeline :browser_auth do
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.EnsureAuthenticated, handler: L5rScheduler.Token
    plug Guardian.Plug.LoadResource
  end

  scope "/", L5rScheduler do
    pipe_through :browser # Use the default browser stack

    get "/",       GameScheduleController, :index
    get "/logout", SessionController,      :delete

    resources "/sessions", SessionController, only: [:new, :create]
    resources "/users",    UserController,    only: [:new, :create]
  end

  scope "/", L5rScheduler do
    pipe_through [:browser, :browser_auth]

    resources "/characters",     CharacterController
    resources "/game_schedules", GameScheduleController
    resources "/users",          UserController, only: [:edit, :update]
  end

  # Other scopes may use custom stacks.
  # scope "/api", L5rScheduler do
  #   pipe_through :api
  # end
end
