// Convert any time set to the time in the client's zone.

export var RemoveButton = {
  attach_handlers: function() {
    $(function() {
      $('.remove-btn').each(function(index, elm) {
        if(elm.dataset['target'] != undefined) {
          $(elm).click(function() {
            $(elm.dataset['target']).remove();
          });
        }
      });
    });
  }
};
