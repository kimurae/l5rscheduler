export var PushToArray = {
  attach_handlers: function() {
    $(function() {
      $('.push-to-array').each(function(index, elm) {
        if(elm.dataset.target != undefined && elm.dataset.name != undefined && elm.dataset.source != undefined) {
          $(elm).click(function() {
            $(elm.dataset.target).append(
              $('<div>').attr({
                class: 'input-group',
                id: $(elm.dataset.target).attr('class') + '-' + $(elm.dataset.target + ' .input-group').length
              }).append(
                $('<input>').attr({
                  class: 'form-control',
                  type: 'text',
                  name: elm.dataset.name,
                  value: $(elm.dataset.source).val()
                })
              ).append(
                $('<span>').attr({
                  class: 'input-group-btn'
                }).append(
                  $('<button>').attr({
                    class: 'btn btn-danger remove-btn',
                    type: 'button'
                  }).data({
                    target: '#' + $(elm.dataset.target).attr('class') + '-' + $(elm.dataset.target).length
                  }).append(
                    $('<span>').attr({
                      class: 'glyphicon glyphicon-remove',
                      'aria-hidden': 'true'
                  }))))).append($('<br>'));
          });
        }
      });
    });
  }
};
