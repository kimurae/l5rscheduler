// Convert any time set to the time in the client's zone.

export var LocalZone = {
  localize: function() {
    $(function() {
      $('.time-utc').each(function(index, elm) {
        if(elm.dataset['time'] != undefined) {
          var hhmmss = elm.dataset['time'].split(':', 3);
          hhmmss[0] = (parseInt(hhmmss[0]) - new Date().getTimezoneOffset() / 60 ).toString();
          $(elm).html(hhmmss.join(':'));
        }
      });
    });
  }
};
