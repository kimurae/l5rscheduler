defmodule L5rScheduler.Token do
  use L5rScheduler.Web, :controller

  def unauthenticated(conn, _params) do
    conn
    |> put_flash(:info, "You must be signed in to access this page")
    |> redirect(to: session_path(conn, :new))
  end

  def unauthorized(conn, _params) do
    conn
    |> put_flash(:error, "You must be the GM to access this page")
    |> redirect(to: game_schedule_path(conn, :index))
  end
end
