defmodule L5rScheduler.CharacterController do
  use L5rScheduler.Web, :controller
  use Guardian.Phoenix.Controller

  alias L5rScheduler.Character

  def index(conn, _params, _user, _claims) do
    characters = Character
      |> Character.by_username
      |> Repo.all
      |> Repo.preload([:game_schedule])

    render(conn, "index.html", characters: characters)
  end

  def new(conn, _params, user, _claims) do
    changeset = user
      |> build_assoc(:characters)
      |> Character.changeset()

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"character" => character_params}, user, _claims) do
    changeset = user
      |> build_assoc(:characters)
      |> Character.changeset(character_params)

    case Repo.insert(changeset) do
      {:ok, _character} ->
        conn
        |> put_flash(:info, "Character created successfully.")
        |> redirect(to: character_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}, _user, _claims) do
    character = Repo.get!(Character, id)
      |> Repo.preload([:game_schedule, :user])

    render(conn, "show.html", character: character)
  end

  def edit(conn, %{"id" => id}, user, _claims) do
    character = user
      |> assoc(:characters)
      |> Repo.get!(id)

    changeset = Character.changeset(character)
    render(conn, "edit.html", character: character, changeset: changeset)
  end

  def update(conn, %{"id" => id, "character" => character_params}, user, _claims) do
    character = user
      |> assoc(:characters)
      |> Repo.get!(id)

    changeset = Character.changeset(character, character_params)

    case Repo.update(changeset) do
      {:ok, character} ->
        conn
        |> put_flash(:info, "Character updated successfully.")
        |> redirect(to: character_path(conn, :show, character))
      {:error, changeset} ->
        render(conn, "edit.html", character: character, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}, user, _claims) do
    character = user
      |> assoc(:characters)
      |> Repo.get!(id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(character)

    conn
    |> put_flash(:info, "Character deleted successfully.")
    |> redirect(to: character_path(conn, :index))
  end
end
