defmodule L5rScheduler.GameScheduleController do
  use L5rScheduler.Web, :controller

  alias L5rScheduler.GameSchedule

  plug Guardian.Plug.EnsurePermissions, [handler: L5rScheduler.Token, gm: ~w(create_schedule)] when action in [:new, :edit, :create, :update]

  def index(conn, _params) do
    game_schedules = Repo.all(GameSchedule)
      |> Repo.preload(characters: from(c in L5rScheduler.Character, order_by: c.name))
      |> Repo.preload(characters: :user)

    render(conn, "index.html", game_schedules: game_schedules)
  end

  def new(conn, _params) do
    changeset = GameSchedule.changeset(%GameSchedule{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"game_schedule" => game_schedule_params}) do
    changeset = GameSchedule.changeset(%GameSchedule{}, game_schedule_params)

    case Repo.insert(changeset) do
      {:ok, _game_schedule} ->
        conn
        |> put_flash(:info, "Game schedule created successfully.")
        |> redirect(to: game_schedule_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    game_schedule = Repo.get!(GameSchedule, id)
    render(conn, "show.html", game_schedule: game_schedule)
  end

  def edit(conn, %{"id" => id}) do
    game_schedule = Repo.get!(GameSchedule, id)
    changeset = GameSchedule.changeset(game_schedule)
    render(conn, "edit.html", game_schedule: game_schedule, changeset: changeset)
  end

  def update(conn, %{"id" => id, "game_schedule" => game_schedule_params}) do
    game_schedule = Repo.get!(GameSchedule, id)
    changeset = GameSchedule.changeset(game_schedule, game_schedule_params)

    case Repo.update(changeset) do
      {:ok, game_schedule} ->
        conn
        |> put_flash(:info, "Game schedule updated successfully.")
        |> redirect(to: game_schedule_path(conn, :show, game_schedule))
      {:error, changeset} ->
        render(conn, "edit.html", game_schedule: game_schedule, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    game_schedule = Repo.get!(GameSchedule, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(game_schedule)

    conn
    |> put_flash(:info, "Game schedule deleted successfully.")
    |> redirect(to: game_schedule_path(conn, :index))
  end
end
