defmodule L5rScheduler.SessionController do
  use L5rScheduler.Web, :controller

  alias L5rScheduler.User

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, %{"session" => %{"username" => username, "password" => password}}) do
    case L5rScheduler.Session.authenticate(username: username, password: password) do
      {:ok, %User{admin: true} = user} ->
        conn
        |> Guardian.Plug.sign_in(user, :access, perms: %{gm: Guardian.Permissions.max})
        |> put_flash(:info, "Successful Login")
        |> redirect(to: game_schedule_path(conn, :index))
      {:ok, %User{admin: false} = user} ->
        conn
        |> Guardian.Plug.sign_in(user, :access, perms: %{default: Guardian.Permissions.max})
        |> put_flash(:info, "Successful Login")
        |> redirect(to: game_schedule_path(conn, :index))
      {:error, message} ->
        conn
        |> put_flash(:error, message)
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> Guardian.Plug.sign_out
    |> redirect(to: "/")
  end
end
