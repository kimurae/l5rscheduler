defmodule L5rScheduler.Repo.Migrations.CreateCharacter do
  use Ecto.Migration

  def change do
    create table(:characters) do
      add :name, :string
      add :avatar, :string
      add :clan, :string
      add :type, :string
      add :game_schedule_id, references(:game_schedules, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:characters, [:game_schedule_id])
    create index(:characters, [:user_id])

  end
end
