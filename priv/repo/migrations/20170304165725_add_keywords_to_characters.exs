defmodule L5rScheduler.Repo.Migrations.AddKeywordsToCharacters do
  use Ecto.Migration

  def change do
    alter table(:characters) do
      add :keywords, {:array, :string}
    end
  end
end
