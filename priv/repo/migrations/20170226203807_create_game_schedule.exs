defmodule L5rScheduler.Repo.Migrations.CreateGameSchedule do
  use Ecto.Migration

  def change do
    create table(:game_schedules) do
      add :label, :string
      add :day, :string
      add :time_of_day, :time

      timestamps()
    end

  end
end
