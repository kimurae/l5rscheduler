use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :l5r_scheduler, L5rScheduler.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :l5r_scheduler, L5rScheduler.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "l5r_scheduler_test",
  pool: Ecto.Adapters.SQL.Sandbox

config :l5r_scheduler, shared_secret: "thingsfallapart"
