# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :l5r_scheduler,
  ecto_repos: [L5rScheduler.Repo]

# Configures the endpoint
config :l5r_scheduler, L5rScheduler.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "WniLJHB6KoPr7WHgFdrjKAdqZHUo8iq10+MdNd+JQhVwiBRfwsqq2LQ4q27lrRYR",
  render_errors: [view: L5rScheduler.ErrorView, accepts: ~w(html json)],
  pubsub: [name: L5rScheduler.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :guardian, Guardian,
  allowed_algos: ["HS512"],
  verify_module: Guardian.JWT,
  issuer: "L5rScheduler",
  ttl: { 30, :days},
  verify_issuer: true,
  secret_key: "WniLJHB6KoPr7WHgFdrjKAdqZHUo8iq10+MdNd+JQhVwiBRfwsqq2LQ4q27lrRYR",
  serializer: L5rScheduler.GuardianSerializer,
  permissions: %{
    default: [
      :use_site
    ],
    gm: [
      :use_site,
      :create_schedule
    ]
  }

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
